#include <stdio.h>
#include "morse.h"
#include "sys/time.h"

#define INVALID_CHAR (-1)

#define DOT '.'
#define DASH '-'
#define BIN_MORSE_CODE_A 0x0100
#define BIN_MORSE_CODE_B 0x1000
#define BIN_MORSE_CODE_C 0x1010
#define BIN_MORSE_CODE_D 0x1000
#define BIN_MORSE_CODE_E 0x0000
#define BIN_MORSE_CODE_F 0x0010
#define BIN_MORSE_CODE_G 0x1100
#define BIN_MORSE_CODE_H 0x0000
#define BIN_MORSE_CODE_I 0x0000
#define BIN_MORSE_CODE_J 0x0111
#define BIN_MORSE_CODE_K 0x1010
#define BIN_MORSE_CODE_L 0x0100
#define BIN_MORSE_CODE_M 0x1100
#define BIN_MORSE_CODE_N 0x1000
#define BIN_MORSE_CODE_O 0x1110
#define BIN_MORSE_CODE_P 0x0110
#define BIN_MORSE_CODE_R 0x0100
#define BIN_MORSE_CODE_S 0x0000
#define BIN_MORSE_CODE_T 0x1000
#define BIN_MORSE_CODE_U 0x0010
#define BIN_MORSE_CODE_W 0x0110
#define BIN_MORSE_CODE_Q 0x1101
#define BIN_MORSE_CODE_X 0x1001
#define BIN_MORSE_CODE_Y 0x1011
#define BIN_MORSE_CODE_Z 0x1100
#define BIN_MORSE_CODE_V 0x0001

static void update_elapsed_time(morse_t* state);
static void start_new_letter(morse_t *state);
static void start_new_word(morse_t *state);
static void start_new_line(morse_t *state);
static void insert_newline_as_whitespace(morse_t *state);
static void insert_space_as_whitespace(morse_t *state);
static void signal_whitespace_char_is_ready(morse_t *state);
static void clear_symbol_seq(morse_t *state);
static char transform_seq_to_char(morse_t *state);
static void signal_letter_is_ready(morse_t *state);



static void update_elapsed_time(morse_t* state)
{
    struct timeval curr;
    gettimeofday(&curr, NULL);
    double elapsedMicroSec = curr.tv_usec - state->prev.tv_usec;
    double elapsedSec = curr.tv_sec - state->prev.tv_sec;
    double elapsed =(elapsedSec*1000)+(elapsedMicroSec / 1000);
    state->prev = curr;
    state->elapsed = elapsed; 
}

static void start_new_letter(morse_t *state)
{
    state->decoded_char = transform_seq_to_char(state);
    signal_letter_is_ready(state);
}

static void start_new_word(morse_t *state)
{
    start_new_letter(state);
    signal_whitespace_char_is_ready(state);
    insert_space_as_whitespace(state);
}
static void start_new_line(morse_t *state)
{
    start_new_letter(state);
    signal_whitespace_char_is_ready(state);
    insert_newline_as_whitespace(state);
}

static char transform_seq_to_char(morse_t *state)
{
    int bin_val = 0x0000;
    int seq_pos = state->current_seq_pos;
    for(int i = 0 ; i < seq_pos ; ++i)
    {
        if(state->symbol_seq[i] == DASH)
        {
            bin_val |= 0x1000 >> (4 * i);
        }
    }
    switch (seq_pos)
    {
    case 1:
        switch (bin_val)
        {
            case BIN_MORSE_CODE_E:
            return 'e';
            case BIN_MORSE_CODE_T:
            return 't';
            default:
            goto error;
        }
    case 2:
    switch (bin_val)
        {
            case BIN_MORSE_CODE_I:
            return 'i';
            case BIN_MORSE_CODE_A:
            return 'a';
            case BIN_MORSE_CODE_N:
            return 'n';
            case BIN_MORSE_CODE_M:
            return 'm';
            default:
            goto error;
        }
    case 3:
    switch (bin_val)
        {
            case BIN_MORSE_CODE_S:
            return 's';
            case BIN_MORSE_CODE_U:
            return 'u';
            case BIN_MORSE_CODE_R:
            return 'r';
            case BIN_MORSE_CODE_W:
            return 'w';
            case BIN_MORSE_CODE_D:
            return 'd';
            case BIN_MORSE_CODE_K:
            return 'k';
            case BIN_MORSE_CODE_G:
            return 'g';
            case BIN_MORSE_CODE_O:
            return 'o';
            default:
            goto error;
        }
    case 4:
    switch (bin_val)
        {
            case BIN_MORSE_CODE_H:
            return 'h';
            case BIN_MORSE_CODE_V:
            return 'v';
            case BIN_MORSE_CODE_F:
            return 'f';
            case BIN_MORSE_CODE_L:
            return 'l';
            case BIN_MORSE_CODE_P:
            return 'p';
            case BIN_MORSE_CODE_J:
            return 'j';
            case BIN_MORSE_CODE_B:
            return 'b';
            case BIN_MORSE_CODE_X:
            return 'x';
            case BIN_MORSE_CODE_C:
            return 'c';
            case BIN_MORSE_CODE_Y:
            return 'y';
            case BIN_MORSE_CODE_Z:
            return 'z';
            case BIN_MORSE_CODE_Q:
            return 'q';
            default:
            goto error;
        }
    default:
        goto error;
    }
    
    error:
    // fprintf(stderr, "Translating seq to char error:");
    // fprintf(stderr, "[%c, %c, %c, %c] n->%d\n", state->symbol_seq[0],state->symbol_seq[1],state->symbol_seq[2],state->symbol_seq[3], seq_pos);
    return '\0';
}

static void signal_letter_is_ready(morse_t *state)
{
    state->char_is_ready = 1;
}

static void signal_whitespace_char_is_ready(morse_t *state)
{
    state->whitespace_char_is_ready = 1;
}

static void insert_newline_as_whitespace(morse_t *state)
{
    state->whitespace_char = '\n';
}

static void insert_space_as_whitespace(morse_t *state)
{
    state->whitespace_char = ' ';
}

static void clear_symbol_seq(morse_t *state)
{
    for(int i = 0 ; i < 4 ; ++i)
    {
        state->symbol_seq[i] = 'x';
    }
}


void morse_init(morse_t* state, int timeunit)
{
    gettimeofday(&(state->prev), NULL);
    clear_symbol_seq(state);
    state->current_seq_pos = 0;
    state->char_is_ready = 0;
    state->decoded_char = '\0';
    state->whitespace_char_is_ready = 0;
    state->whitespace_char = '\0';
    morse_set_time_unit(state, timeunit);
}

void morse_push_down(morse_t *state)
{
    update_elapsed_time(state);
    double elapsed = state->elapsed;
    if(elapsed < state->between_symbol_time) // still writing the same letter
    {
        return;
    }
    else if(elapsed < state->between_letters_time) // starting new word (should return 2 letters!) 
    {
        start_new_letter(state);
    }
    else if(elapsed < state->between_words_time) //starting new word
    {
        start_new_word(state);
    }
    else //new line
    {
        start_new_line(state);
    }
    
}

void morse_release(morse_t* state)
{
    update_elapsed_time(state);
    double elapsed = state->elapsed;
    if(state->current_seq_pos < 4)
    {
        if(elapsed < state->dot_time)
        {
            state->symbol_seq[state->current_seq_pos] = DOT;
        }
        else
        {
            state->symbol_seq[state->current_seq_pos] = DASH;
        }
        state->current_seq_pos++;
    }
}

int morse_is_char_avaible(morse_t* state)
{
    return state->char_is_ready;
}

char morse_get_char(morse_t* state)
{
    char c = state->decoded_char;
    state->decoded_char = '\0';
    state->char_is_ready = 0;
    clear_symbol_seq(state);
    state->current_seq_pos = 0;
    return c;
}

int morse_is_whitespace_avaible(morse_t* state)
{
    return state->whitespace_char_is_ready;
}
char morse_get_whitespace(morse_t *state)
{
    char c = state->whitespace_char;
    state->whitespace_char = '\0';
    state->whitespace_char_is_ready = 0;
    return c;
}

void morse_show_state_info_debug(morse_t* state)
{
    printf("[%c,%c,%c,%c] seq_pos: %d decoded_char: %c\n",state->symbol_seq[0],state->symbol_seq[1],state->symbol_seq[2],state->symbol_seq[3],state->current_seq_pos,state->decoded_char);
}

void morse_show_state_info(morse_t* state)
{
    printf("[%c,%c,%c,%c]",state->symbol_seq[0],state->symbol_seq[1],state->symbol_seq[2],state->symbol_seq[3]);
}

void morse_set_time_unit(morse_t *state, int miliseconds)
{
    int timeunit = miliseconds;
    state->timeunit = timeunit;
    state->between_symbol_time = timeunit;
    state->between_letters_time = timeunit * 3;
    state->between_words_time = timeunit * 7;
    state->dot_time = timeunit;
    state->dash_time = timeunit*3;
}

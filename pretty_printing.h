#include <stdio.h>

void set_cursor_pos(int row, int col);
void erase_rest_of_line();
void clear_screen();
void save_cursor_pos();
void restore_cursor_pos();


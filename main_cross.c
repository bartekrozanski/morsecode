#include "morse.h"
#include <stdio.h>
#include <unistd.h>
#include <gpiod.h>
#include "pretty_printing.h"

void usage()
{
    printf("USAGE\n");
    printf("[Time unit in milisec] [Max bouncing time in milisec to handle] [debug (0 for off)]");
}

int main(int argc, char **argv)
{
    struct gpiod_chip *chip_bank;
    struct gpiod_line *input_line;
    struct gpiod_line *output_line;
    struct gpiod_line_event event;
    int output_line_num = 31;
    int input_line_num = 13;

    chip_bank = gpiod_chip_open_by_name("gpiochip1");
    if(chip_bank == NULL)
    {
        perror("Could not open chip by name");
    }
    output_line = gpiod_chip_get_line(chip_bank, output_line_num);
    if(output_line == NULL)
    {
        perror("Could not get line for output");
    }
    input_line = gpiod_chip_get_line(chip_bank, input_line_num);
    if(input_line == NULL)
    {
        perror("Could not get line for input");
    }

    int ret;
    ret = gpiod_line_request_output(output_line, "Morse app", GPIOD_LINE_ACTIVE_STATE_LOW);
    if(ret<0)
    {
     perror("Request output for line failed\n");
     return EXIT_FAILURE;   
    }

    ret = gpiod_line_request_both_edges_events(input_line, "Morse app");
    if(ret < 0)
    {
        perror("Request for input events on line failed\n" );
        return EXIT_FAILURE;
    }
int timeunit;
int event_wait;
struct timespec event_wait_time;
event_wait_time.tv_sec = 0;

if(argc != 3)
{
    usage();
    return EXIT_FAILURE;
}
else
{
	timeunit = atoi(argv[1]);
    event_wait_time.tv_nsec = atoi(argv[2]) * (int)1e6;
}
	morse_t state;
    morse_init(&state, timeunit);
    clear_screen();
    set_cursor_pos(3,1);
    printf("Morse app started!:\n");
    while(1)
    {
        event_wait = gpiod_line_event_wait(input_line, NULL);
        if(event_wait == -1)
        {
            perror("Infinite wait for input event failed");
            continue;
        }
        if(gpiod_line_event_read(input_line, &event) != 0)
        {
            perror("Could not read event from input line");
            continue;
        }
        while(event_wait != 0)
        {
            event_wait = gpiod_line_event_wait(input_line, &event_wait_time);
            if(event_wait == 0)
            {
                break;
            }
            if(event_wait == -1)
            {
                perror("Bouncing-handling wait for input event failed");
                continue;
            };
            if(gpiod_line_event_read(input_line, &event) != 0)
            {
                perror("Could not read event from input line");
                continue;
            }
        }
        
        if(event.event_type == GPIOD_LINE_EVENT_RISING_EDGE) //release
        {
            if(gpiod_line_set_value(output_line, 0) < 0)
            {
                perror("Could not set value at output line to low after release event");
            }
            morse_release(&state);

        }
        if(event.event_type == GPIOD_LINE_EVENT_FALLING_EDGE) //push down
        {
            if(gpiod_line_set_value(output_line, 1))
            {
                perror("Could not set value at output line to high after pushdown event");
            }
            morse_push_down(&state);
        }
        if(morse_is_char_avaible(&state))
        {
            printf("%c", morse_get_char(&state));
	        fflush(stdout);
        }
        if(morse_is_whitespace_avaible(&state))
        {
            printf("%c", morse_get_whitespace(&state));
            fflush(stdout);
        }
        save_cursor_pos();
        set_cursor_pos(1, 10);
        erase_rest_of_line();
        morse_show_state_info(&state);
        restore_cursor_pos();
        fflush(stdout);
    }
    return EXIT_SUCCESS;
}

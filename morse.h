#include <sys/time.h>


typedef struct morse
{
    struct timeval prev;
    double elapsed;
    char symbol_seq[4];
    int current_seq_pos;
    int char_is_ready;
    char decoded_char;
    int whitespace_char_is_ready;
    char whitespace_char;
    int timeunit;
    int between_symbol_time;
    int between_letters_time;
    int between_words_time;
    int dot_time;
    int dash_time;
}morse_t;


void morse_init(morse_t* state, int timeunit);
void morse_push_down(morse_t* state);
void morse_release(morse_t* state);
int morse_is_char_avaible(morse_t* state);
char morse_get_char(morse_t* state);
int morse_is_whitespace_avaible(morse_t* state);
char morse_get_whitespace(morse_t* state);
void morse_show_state_info_debug(morse_t* state);
void morse_show_state_info(morse_t* state);
void morse_set_time_unit(morse_t *state, int miliseconds);


#include "pretty_printing.h"

void set_cursor_pos(int row, int col)
{
    printf("\x1b[%d;%dH", row, col);
}

void erase_rest_of_line()
{
    printf("\x1b[0K");
}

void clear_screen()
{
    printf("\x1b[2J");
}

void save_cursor_pos()
{
    printf("\x1b[s");
}

void restore_cursor_pos()
{
    printf("\x1b[u");
}



CC=$(CROSS_COMPILE)gcc #iff cross_compile is empty then it is just gcc
OBJS := morse.o pretty_printing.o
CFLAGS = -Wall
morse_cross: $(OBJS) main_cross.o
	$(CC) -o morse_cross $(CFLAGS) $(LDFLAGS) $(OBJS) main_cross.o -lgpiod

morse_test: $(OBJS) main_test.o
	$(CC) -o morse_test $(CFLAGS) $(LDFLAGS) $(OBJS) main_test.o

$(OBJS) : %.o : %.c
	$(CC) -c $(CFLAGS) $< -o $@

main_test.o:
	$(CC) -c $(CFLAGS) main_test.c -o main_test.o

main_cross.o:
	$(CC) -c $(CFLAGS) main_cross.c -o main_cross.o

clean:
	rm *.o morse_test morse_cross

